JcdBrcNT24IFN: Prototype for wrap and reuse with Camel
======================================================
Author: Nevin Zhu  
Technologies: Camel, Spring, JMS, Java 
Summary: The prototype demonstrates how to create and invoke a JCAP otd using camel and spring configuration 

What is it?
-----------

The `prototype` demonstrates how to create a camel route that invokes OTD service with minimal code changes to the existing code in Red Hat JBoss Enterprise Application Platform.

The following actions are performed in the camel route:

1. Input data is retrieved from a JMS queue
2. Data is passed to the java bean for transformation
3. Transformed data is send to a JMS queue

System requirements
-------------------

The application this project produces is designed to be run on Red Hat JBoss Enterprise Application Platform 6.4 or later. 

All you need to build this project is Java 7.0 (Java SDK 1.7) or later, Maven 3.0 or later and JBoss A-MQ 6.0 or later.

 
Configure Maven
---------------

If you have not yet done so, you must configure Maven and setup the JCAP dependencies by importing them into the maven repository. 
These are 3rd party jars that are not built by maven and not available on Maven Central Repo. Following is an example. Each artifact must be imported individually.
At this writing there are 

mvn install:install-file -Dfile=com.stc.otd.fwrunapi.jar -DgroupId=jcaps-import -Dversion=1.0 -DartifactId=com.stc.otd.fwrunapi -Dpackaging=jar

For the purpose of this prototype, a zip file is included in the root directory called jcap-import.zip. Place the content in the zip file in your .m2/repository folder. This will ensure maven to build the project without any errors.

In the following instructions, replace `EAP_HOME` with the actual path to your JBoss EAP 6 installation.

Enable JBoss EAP Logging for Camel
-----------------------------------

For Linux:  EAP_HOME/bin/standalone.conf
For Windows: EAP_HOME\bin\standalone.conf.bat

1. Add the following line 

JAVA_OPTS="$JAVA_OPTS -Dorg.jboss.as.logging.per-deployment=false"

2. EAP_HOME/standalone/configuration/standalone.xml
3. Navigate to <subsystem xmlns="urn:jboss:domain:logging:1.5">
4. add the following this subsystem

<logger category="org.apache.camel">
    <level name="INFO"/>
</logger>

Start JBoss A-MQ Server
--------------------------

For Linux:  FUSE_HOME/bin/fuse.sh
For Windows: FUSE_HOME\bin\fuse.bat

Start the JBoss EAP Server
-------------------------

1. Open a command prompt and navigate to the root of the JBoss EAP directory.
2. The following shows the command line to start the server:

        For Linux:   EAP_HOME/bin/standalone.sh
        For Windows: EAP_HOME\bin\standalone.bat


Build and Deploy the application
-------------------------

1. Make sure you have started the JBoss EAP server as described above.
2. Open a command prompt and navigate to the root directory of this application.
3. Type this command to build and deploy the archive:

        mvn clean install

4. Copy the target/nt24ifn.war into the EAP_HOME/standalone/deployment folder        
5. This will deploy `target/nt24ifn.war` to the running instance of the server.

Check the EAP_HOME/standalone/log/server.log that no error exist and the camel route are deployed successfully.

Access the application
------------------------

1. Open a browser and navigate to http://localhost:8181/hawtio (Default URL for the Fuse Management Console) 
2. Navigate to the ActivemMQ tab > DATA_IN queue
3. Place the following string into the DATA_IN queue and hit SEND.

<?xml version="1.0" encoding="utf-8"?><NurseTriage24x7><Record><EDIReferenceNumber>391523</EDIReferenceNumber><TransactionId>DKR0000Q</TransactionId><CallerFirstName>Testfirst</CallerFirstName><CallerLastName>Callos Resource</CallerLastName><SupervisorFirstName>TEST</SupervisorFirstName><SupervisorLastName>TEST</SupervisorLastName><SupervisorTelephoneNumber>2101112222</SupervisorTelephoneNumber><TransactionCallType>Inbound</TransactionCallType><TransactionCallDate>20140911</TransactionCallDate><TransactionCallTime>093527</TransactionCallTime><TransactionCallTimeZone>CDT</TransactionCallTimeZone><TransactionCallPathway>Triage</TransactionCallPathway><RelationshipToCaller></RelationshipToCaller><CallReason>Testing the POI Verbiage wording and ESIS/IFN translation</CallReason><Jurisdiction>TX</Jurisdiction><InjuryDescription></InjuryDescription><PatientFirstName>Testfirst</PatientFirstName><PatientMiddleName>xx</PatientMiddleName><PatientLastName>Callos Resource</PatientLastName><PatientAddress>123 Test Street</PatientAddress><PatientCity>San Antonio</PatientCity><PatientState>TX</PatientState><PatientZip>78251</PatientZip><PatientWorkTelephone></PatientWorkTelephone><PatientHomeTelephone>2101112222</PatientHomeTelephone><PatientSSN>111221111</PatientSSN><PatientGender>F</PatientGender><PatientDateOfBirth>19700101</PatientDateOfBirth><PatientLanguage>*English</PatientLanguage><EmployeeID>123456</EmployeeID><JobTitle>Maintenance/Engineering</JobTitle><OtherJob></OtherJob><HourlyWage>0</HourlyWage><AverageHoursWorked>0</AverageHoursWorked><PayorLocation>0</PayorLocation><BusinessIndustryID>CALLO</BusinessIndustryID><BusinessIndustryAccountID>CLS</BusinessIndustryAccountID><BusinessIndustryName>CALLOS RESOURCE</BusinessIndustryName><BusinessIndustryAddress></BusinessIndustryAddress><BusinessIndustryCity></BusinessIndustryCity><BusinessIndustryState></BusinessIndustryState><BusinessIndustryZip></BusinessIndustryZip><BusinessIndustryTelephoneNumber></BusinessIndustryTelephoneNumber><BusinessIndustryFaxNumber></BusinessIndustryFaxNumber><ClientID>838000</ClientID><ClientName>CALLOS RESOURCE - DEFAULT LOCATION - DEFAULT</ClientName><ClientAddress></ClientAddress><ClientCity></ClientCity><ClientState></ClientState><ClientZip></ClientZip><ClientTelephoneNumber></ClientTelephoneNumber><ClientFaxNumber></ClientFaxNumber><ExternalAccountNumber>29605166</ExternalAccountNumber><DivisionCode></DivisionCode><RegionCode></RegionCode><Interpreter>NR</Interpreter><NumberofDependents>0</NumberofDependents><PatientFaxNumber></PatientFaxNumber><PatientEmail></PatientEmail><PatientAddress2></PatientAddress2><PatientCounty>Bexar</PatientCounty><PatientCellNumber></PatientCellNumber><PatientAge>45</PatientAge><PatientAgeMonths>4</PatientAgeMonths><MaritalStatus></MaritalStatus><HireDate>20140904</HireDate><EmploymentStatus></EmploymentStatus><JobCode>MAINT</JobCode><ReportedByPhoneNo></ReportedByPhoneNo><DisabilityStartDate></DisabilityStartDate><PaidinFullIndicator></PaidinFullIndicator><InjuryID></InjuryID><Filler01></Filler01><Filler02></Filler02><Filler03></Filler03><Filler04></Filler04><Filler05></Filler05><Survey></Survey><Injury><TriageRecordID>DKR0000Q</TriageRecordID><TriageCallDate>20140911</TriageCallDate><TriageCallTime>093600</TriageCallTime><TriageCallTimeZone>CDT</TriageCallTimeZone><InjuryMainComplaint>ACHES</InjuryMainComplaint><RelatedComplaint></RelatedComplaint><RelatedComplaint2></RelatedComplaint2><RelatedComplaint3></RelatedComplaint3><HealthHistory></HealthHistory><InjuryDateTime>20140911 093715</InjuryDateTime><InjuryType>CRUSHING</InjuryType><InjuredBodyPart>EAR(S)</InjuredBodyPart><InjuryLocation>Lobby</InjuryLocation><WeightLifted>0</WeightLifted><HeightofFall>0</HeightofFall><EquipmentUtilized>Fork Lift</EquipmentUtilized><AssistiveDevice>Shoes for Crews</AssistiveDevice><Predisposition>Call 911</Predisposition><Disposition>Go to Urgent Care</Disposition><OverrideLevel></OverrideLevel><OverrideReason></OverrideReason><Compliance>Yes</Compliance><NonComplianceReason></NonComplianceReason><FollowUpDisposition></FollowUpDisposition><CallNotes>TEST Notes</CallNotes><WitnessName>None</WitnessName><GuidelineUsed1>Back Pain (Adult After Hours)</GuidelineUsed1><GuidelineUsed2></GuidelineUsed2><GuidelineUsed3></GuidelineUsed3><GuidelineUsed4></GuidelineUsed4><ProviderName></ProviderName><ProviderType></ProviderType><ProviderAddress></ProviderAddress><ProviderCity></ProviderCity><ProviderState></ProviderState><ProviderZip></ProviderZip><ProviderTelephoneNumber></ProviderTelephoneNumber><RecordingNumber>123456</RecordingNumber><AdditionalRecordingNumber></AdditionalRecordingNumber><AssistiveDevice2></AssistiveDevice2><AssistiveDevice3></AssistiveDevice3><InjuryTimeZone>Central Daylight Time</InjuryTimeZone><NotifiedDateTime>2014-09-11 09:36:00</NotifiedDateTime><NotifiedTimeZone>Central Standard Time</NotifiedTimeZone><MilestoWork>0</MilestoWork><InjuredOnPremises>Yes</InjuredOnPremises><AccidentAddress></AccidentAddress><AccidentCity></AccidentCity><AccidentState></AccidentState><AccidentZip></AccidentZip><ReturnToWorkProgram></ReturnToWorkProgram><WitnessWorkTelephone></WitnessWorkTelephone><HCRINo></HCRINo><InitialTreatmentCode></InitialTreatmentCode><BodyPartCode>13</BodyPartCode><InjuryCode>13</InjuryCode><CauseOfInjuryCode>13</CauseOfInjuryCode><CauseOfInjury>CAUGHT IN, UNDER, OR BETWEEN: MISCELLANEOUS</CauseOfInjury><CallingFrom></CallingFrom><WorkLocationCode></WorkLocationCode><WorkLocation></WorkLocation><PrivateArea>No</PrivateArea><Validity></Validity><SideofBody>Left</SideofBody><ProviderName2></ProviderName2><ProviderType2></ProviderType2><ProviderAddress2></ProviderAddress2><ProviderCity2></ProviderCity2><ProviderState2></ProviderState2><ProviderZip2></ProviderZip2><ProviderTelephoneNumber2></ProviderTelephoneNumber2><ProviderName3></ProviderName3><ProviderType3></ProviderType3><ProviderAddress3></ProviderAddress3><ProviderCity3></ProviderCity3><ProviderState3></ProviderState3><ProviderZip3></ProviderZip3><ProviderTelephoneNumber3></ProviderTelephoneNumber3><TextPermission></TextPermission><TextCellPhone></TextCellPhone><TextProvider></TextProvider><TextNotSentReason></TextNotSentReason><Filler06></Filler06><Filler07></Filler07><Filler08></Filler08><Filler09></Filler09><Filler10></Filler10></Injury></Record></NurseTriage24x7>
  
4. Observe the server.log and verify the data has been transformed and sent to outbound JMS queue

Debug the Application
------------------------

If you want to debug the source code of any library in the project, run the following command to pull the source into your local repository. The IDE should then detect it.

    mvn dependency:sources
   

