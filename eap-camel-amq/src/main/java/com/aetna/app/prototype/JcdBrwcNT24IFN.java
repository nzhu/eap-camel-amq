package com.aetna.app.prototype;
/**
*
* Core transformation logic for message
* 
* Refactored to interface with Camel Route
* 
* @author Nevin Zhu
* @since  Dec 10, 2015
*
*/

import xsd.cvtyStdEvent_795650394.ExStdEvent;
import xsd.cvtyStdEvent_795650394.PayloadSectionType;
import xsd.eX_TPAttribute1628188874.TPAttribute;

import org.apache.commons.lang3.StringUtils; //.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.commons.io.FilenameUtils;
import java.util.Date;
import org.apache.commons.lang3.ArrayUtils;
import xsd.EDI_1226447247.X_sequence_B;
import com.stc.targetFields1442425817.Env2.Claim;


public class JcdBrwcNT24IFN
{

    public com.stc.codegen.logger.Logger logger;

    public com.stc.codegen.alerter.Alerter alerter;

    public com.stc.codegen.util.CollaborationContext collabContext;

    public com.stc.codegen.util.TypeConverter typeConverter;

    public static final String FILE = "FILE";

    public static final String OUT = "_out";

    public static final String ERROR_TIMESTAMP_FORMAT = "MMddyyyy HH:mm:ss";

    public static final String[] IN_DATE_FORMAT = new String[]{ "yyyyMMdd", "yyyyMMdd HHmmss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "M/d/yyyy", "MMddyyyy" };

    public static final String OUT_DATE_FORMAT = "yyyyMMdd";

    public static final String OUT_TIME_FORMAT = "HHmm";

    public static final String NO_EQUIPMENT = "NA,NONE";

    public static final String OTHER_EQUIPMENT = "OTHER";

    public static final String[] WITNESS_FILTER = new String[]{ "NONE", "N/A", "NA", "" };

    public static final String[] CLAIMTYPE_R_FILTER = new String[]{ "CALL DOCTOR (NON-W/C ONLY)", "GO TO LAB", "REI INCIDENT REPORT", "SELF-CARE", "UNABLE TO COMPLETE CALL" };

    public static final String CANADA_ZIP_EXP = "[A-Z]\\d[A-Z]\\d[A-Z]\\d";

    public static final String REC_DELIM = "\r\n";

    public static final String CLIENT_ABM = "ABM";

    public static final String SOURCE_SYSTEM = "NT24";

    public static final String TARGET_SYSTEM = "CLIENT";

    public static final String[] IGNORE_INJURY_CODE = new String[]{ "1", "01", "42", "79", "90", "91", "" };

    public static final String REFERRED_BY_WHOM = "COVENTRY";

    public static final String[] SURVEY_ANSWER = new String[]{ "Y", "N", "U" };

    public static final String NO_TAG = "N/A";

    public static final String ASI_ABM_ADR = "ASI:ABM_ADR";
    
    public byte[] wrap(byte[] msg) throws Throwable {

        TPAttribute attrib = new TPAttribute();

        attrib.setINFILE("infile");

        attrib.setFILETRACKERID("cameltest_filetrackerid");

        attrib.setXFILENAME("xfilename");

        attrib.setEVENTNAME("camelEventName");

        ExStdEvent exStdEvent = new ExStdEvent();

        exStdEvent.setId("cameltest_filetrackerid");

        exStdEvent.getContainer( 0 ).getMetaDataSection().getEvent().setTradingPartnerName("BR4_CNAMBRV4");

        exStdEvent.getContainer( 0 ).getPayloadSection().setRawData(msg);

        exStdEvent.getContainer( 0 ).getPayloadSection().getUserData( 0 ).getData().setExtAttributeValueXML(attrib.marshalToString());

        return exStdEvent.marshalToBytes();

    }
    
    public void receive(StringBuffer sb, String inputStr, com.stc.connectors.jms.Message input, com.stc.targetFields1442425817.Root otdBrwcNT24IFN, xsd.eX_TPAttribute1628188874.TPAttribute otdTPAttribute, xsd.cvtyStdEvent_795650394.ExStdEvent ExStdEvent_ExStdEvent, com.stc.connectors.jms.JMS jmsOut, com.stc.connectors.jms.JMS jmsProcessHandler, xsd.EDI_1226447247.NurseTriage24x7 otdBrwcNT24, xsd.xsdProcessHandler_1006081694.ProcessHandler otdProcessHandler, otdDbNT24IFN.OtdDbNT24IFNOTD otdDbNT24IFN )
        throws Throwable
    {
       // this.logger.info( "+++++ Start " + this.collabContext.getCollaborationName() );
        String tpLogical = null;
        String trackingId = null;
        try {
            ExStdEvent exStdEventIn = new ExStdEvent();
            exStdEventIn.unmarshalFromString( inputStr );
            ExStdEvent exStdEventOut = new ExStdEvent();
        
            otdProcessHandler.setProc_Tracker_Id( exStdEventIn.getId() );
           // otdProcessHandler.setEvent_Source( this.collabContext.getCollaborationName() );
            for (int x = 0; x < exStdEventIn.countContainer(); x++) {
                tpLogical = exStdEventIn.getContainer( x ).getMetaDataSection().getEvent().getTradingPartnerName();
                exStdEventOut.getContainer( x ).getPayloadSection().getUserData( 0 ).getData().setExtAttributeValueXML( exStdEventIn.getContainer( x ).getPayloadSection().getUserData( 0 ).getData().getExtAttributeValueXML() );
                otdTPAttribute.unmarshalFromString( exStdEventIn.getContainer( x ).getPayloadSection().getUserData( 0 ).getData().getExtAttributeValueXML() );
                trackingId = otdTPAttribute.getFILETRACKERID();
                otdProcessHandler.setTP_Logical( tpLogical );
                otdProcessHandler.setOriginal_TP( tpLogical );
                otdProcessHandler.setSf_Tracker_Id( otdTPAttribute.getFILETRACKERID() );
                String validationResult = validateTpAttribute( otdTPAttribute );
                if (validationResult == null) {
                    byte[] msgIn = null;
                    if (exStdEventIn.getContainer( x ).getPayloadSection().getReference().equals( "FILE" )) {
                        java.io.File sourcePath = new java.io.File( new String( exStdEventIn.getContainer( x ).getPayloadSection().getRawData() ) );
                        // change file handling if payload becomes too big
                        msgIn = org.apache.commons.io.FileUtils.readFileToByteArray( sourcePath );
                    } else {
                        msgIn = exStdEventIn.getContainer( x ).getPayloadSection().getRawData();
                    }
                    otdBrwcNT24IFN = map( msgIn, otdDbNT24IFN );
                    if (otdBrwcNT24IFN.countClaim() > 0) {
                        String out = otdBrwcNT24IFN.marshalToString() + REC_DELIM;
                        String businessAccountId = otdBrwcNT24IFN.getClaim( 0 ).getClam().getVendorClientIdCode().toUpperCase().trim();
                        exStdEventOut.getContainer( x ).getPayloadSection().setReference( "" );
                        String inFile = otdTPAttribute.getINFILE();
                        otdTPAttribute.setINFILE( FilenameUtils.concat( FilenameUtils.getFullPath( inFile ), businessAccountId ) );
                        exStdEventOut.getContainer( x ).getPayloadSection().getUserData( 0 ).getData().setExtAttributeValueXML( otdTPAttribute.marshalToString() );
                        exStdEventOut.getContainer( x ).getPayloadSection().setRawData( out.getBytes() );
                        exStdEventOut.getContainer( x ).getMetaDataSection().getEvent().setTradingPartnerName( tpLogical + OUT );
                        // send success update event
                        otdProcessHandler.getProcessTrackingRec().getX_sequence_C().setActivity_Type( 2 );
                        otdProcessHandler.getProcessTrackingRec().getX_sequence_C().setActivity_Desc( "Mapping complete" );
                        // Added by Nevin
                        sb.append(otdProcessHandler.marshalToString());
                        /*
                        jmsProcessHandler.sendText( otdProcessHandler.marshalToString() );
                        jmsOut.sendText( exStdEventOut.marshalToString() );
                        */
                    }
                } else {
                    logger.error( "+++++ -- Missing TP Attribute" );
                    errorEvent( jmsProcessHandler, otdProcessHandler, otdTPAttribute, "6", validationResult );
                }
            }
        } catch ( com.stc.otd.runtime.UnmarshalException umex ) {
            logger.error( "+++++ -- Error occurred in collaboration." + umex.getMessage() );
            errorEvent( jmsProcessHandler, otdProcessHandler, otdTPAttribute, "8", umex.toString() );
            umex.printStackTrace();
        } catch ( Exception e ) {
            logger.error( "+++++ -- Error occurred in collaboration." + e.getMessage() );
            errorEvent( jmsProcessHandler, otdProcessHandler, otdTPAttribute, "5", e.toString() );
            e.printStackTrace();
        }
       // this.logger.info( "+++++ End " + this.collabContext.getCollaborationName() );
    }

    // -----------------------------------------------------
    public com.stc.targetFields1442425817.Root map( byte[] msgIn, otdDbNT24IFN.OtdDbNT24IFNOTD otdDbNT24IFN )
        throws Exception
    {
        xsd.EDI_1226447247.NurseTriage24x7 otdBrwcNT24 = new xsd.EDI_1226447247.NurseTriage24x7();
        otdBrwcNT24.unmarshalFromBytes( msgIn );
        com.stc.targetFields1442425817.Root otdBrwcNT24IFN = new com.stc.targetFields1442425817.Root();
        String strMsg = new String( msgIn );
        String[] strRecords = strMsg.split( "</Record>" );
        String ediReferenceNumber = "";
        String accountID = "";
        try {
            // Loop thru records
            for (int i = 0; i < otdBrwcNT24.countRecord(); i++) {
                ediReferenceNumber = otdBrwcNT24.getRecord( i ).getX_sequence_B().getEDIReferenceNumber();
                accountID = StringUtils.trimToEmpty( otdBrwcNT24.getRecord( i ).getX_sequence_B().getBusinessIndustryAccountID() ).trim();
                if (accountID.isEmpty()) {
                    throw new Exception( "Missing Business Industry Account ID!" );
                }
                // CLAM
                mapCLAM( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // CLED
                mapCLED( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // DTWC
                mapDTWC( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // EEDT
                mapEEDT( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // SSQD
                mapSSQD( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // HOSD
                if (StringUtils.isNotBlank( otdBrwcNT24.getRecord( i ).getX_sequence_B().getInjury( 0 ).getX_sequence_D().getProviderName() ) || StringUtils.isNotBlank( otdBrwcNT24.getRecord( i ).getX_sequence_B().getInjury( 0 ).getX_sequence_D().getProviderName() )) {
                    mapHOSD( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                }
                // WITS
                mapWITS( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
                // ASID
                //mapASID( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B(), otdDbNT24IFN, strRecords[i] );
                // COND
                mapCOND( otdBrwcNT24IFN.getClaim( i ), otdBrwcNT24.getRecord( i ).getX_sequence_B() );
            }
        } catch ( Exception e ) {
        	e.printStackTrace();
            throw new Exception( "EDIReferenceNumber: [" + ediReferenceNumber + "]. " + e.toString() );
        }
        return otdBrwcNT24IFN;
    }

    private void mapCLAM( Claim ifnData, X_sequence_B record )
        throws java.text.ParseException
    {
        ifnData.getClam().setVendorClientIdCode( record.getBusinessIndustryAccountID() );
        ifnData.getClam().setIncidentNumber( record.getTransactionId() );
        ifnData.getClam().setClientAccountNumber( record.getExternalAccountNumber() );
        ifnData.getClam().setClientUnitNumber( StringUtils.upperCase( record.getPayorLocation() ) );
        Date dt = DateUtils.parseDate( record.getInjury( 0 ).getX_sequence_D().getInjuryDateTime(), IN_DATE_FORMAT );
        ifnData.getClam().setDateInjuryOccurred( DateFormatUtils.format( dt, OUT_DATE_FORMAT ) );
        ifnData.getClam().setTimeInjuryOccurred( DateFormatUtils.format( dt, OUT_TIME_FORMAT ) );
        ifnData.getClam().setEventTimeAmpm( DateFormatUtils.format( dt, "a" ) );
        String disposition = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getDisposition() );
        if (StringUtils.isNotEmpty( disposition ) && ArrayUtils.contains( CLAIMTYPE_R_FILTER, disposition.toUpperCase() )) {
            ifnData.getClam().setClaimType( "R" );
        } else {
            ifnData.getClam().setClaimType( "C" );
        }
        ifnData.getClam().setAccidentLocationName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getInjuryLocation() ) ) );
        String accidentAddress1 = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getAccidentAddress() );
        if (accidentAddress1.isEmpty()) {
            ifnData.getClam().setAccidentAddress1( "UNKNOWN" );
        } else {
            ifnData.getClam().setAccidentAddress1( accidentAddress1.toUpperCase() );
        }
        ifnData.getClam().setAccidentCity( StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getAccidentCity() ) ) );
        ifnData.getClam().setAccidentState( StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getAccidentState() ) ) );
        ifnData.getClam().setAccidentPostalCode( checkZip( record.getInjury( 0 ).getX_sequence_D().getAccidentZip() ) );
        ifnData.getClam().setSystemDate( DateFormatUtils.format( new java.util.Date(), OUT_DATE_FORMAT ) );
    }

    // mapCLAM
    private void mapCLED( Claim ifnData, X_sequence_B record )
    {
        ifnData.getCled().setIncidentNumber( record.getTransactionId() );
        ifnData.getCled().setEventDescriptionSequenceNum( "000" );
        ifnData.getCled().setAccidentLossDescription( StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjuryDescription() ) ) );
    }

    private void mapDTWC( Claim ifnData, X_sequence_B record )
    {
        ifnData.getDtwc().setIncidentNumber( record.getTransactionId() );
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getInjuredOnPremises() )) {
            ifnData.getDtwc().setInjuredOnPremises( record.getInjury( 0 ).getX_sequence_D().getInjuredOnPremises() );
        } else {
            ifnData.getDtwc().setInjuredOnPremises( "U" );
        }
        ifnData.getDtwc().setInitialTreatment( record.getInjury( 0 ).getX_sequence_D().getInitialTreatmentCode() );
        ifnData.getDtwc().setEmployerNotifiedDate( record.getInjury( 0 ).getX_sequence_D().getTriageCallDate() );
        String injuryCode = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getInjuryCode() );
        if (!ArrayUtils.contains( IGNORE_INJURY_CODE, injuryCode )) {
            ifnData.getDtwc().setInjuryCode( StringUtils.leftPad( injuryCode, 2, '0' ) );
        }
        String ncciCause = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getCauseOfInjuryCode() );
        if (!StringUtils.startsWithAny( ncciCause, new String[]{ "21", "22", "83" } )) {
            ifnData.getDtwc().setNcciDirectCauseOfInjury( ncciCause );
        }
        ifnData.getDtwc().setNcciBodyPart( StringUtils.left( StringUtils.stripStart( record.getInjury( 0 ).getX_sequence_D().getBodyPartCode(), "0" ), 4 ) );
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getSideofBody() )) {
            ifnData.getDtwc().setSideOfBody( StringUtils.left( record.getInjury( 0 ).getX_sequence_D().getSideofBody(), 2 ).toUpperCase() );
        }
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getReturnToWorkProgram() )) {
            ifnData.getDtwc().setReturnToWorkProgram( record.getInjury( 0 ).getX_sequence_D().getReturnToWorkProgram() );
        } else {
            ifnData.getDtwc().setReturnToWorkProgram( "U" );
        }
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getCallingFrom() )) {
            ifnData.getDtwc().setReportPreparedByWorkPhoneNumber( checkPhone( record.getInjury( 0 ).getX_sequence_D().getCallingFrom() ) );
        }
        ifnData.getDtwc().setDatePrepared( record.getInjury( 0 ).getX_sequence_D().getTriageCallDate() );
        String witnessName = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getWitnessName() ) );
        if (ArrayUtils.contains( WITNESS_FILTER, witnessName )) {
            ifnData.getDtwc().setWitnesses( "N" );
        } else {
            ifnData.getDtwc().setWitnesses( "Y" );
        }
        // get assistive device
        java.util.Set devices = new java.util.HashSet();
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice() )) {
            devices.add( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice() );
        }
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice2() )) {
            devices.add( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice2() );
        }
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice3() )) {
            devices.add( record.getInjury( 0 ).getX_sequence_D().getAssistiveDevice3() );
        }
        String safetyEquipUsed = "Y";
        if (devices.isEmpty()) {
            safetyEquipUsed = "U";
        } else {
            java.util.Iterator it = devices.iterator();
            while (it.hasNext()) {
                String device = StringUtils.trimToEmpty( (String) it.next() ).toUpperCase();
                if (StringUtils.isNotEmpty( device )) {
                    if (StringUtils.contains( NO_EQUIPMENT, device )) {
                        safetyEquipUsed = "N";
                    } else if (StringUtils.contains( OTHER_EQUIPMENT, device )) {
                        safetyEquipUsed = "U";
                    }
                }
            }
        }
        ifnData.getDtwc().setSafeguardSafetyEquipmentUsed( safetyEquipUsed );
        if (StringUtils.isNotEmpty( record.getInjury( 0 ).getX_sequence_D().getValidity() )) {
            String validity = StringUtils.left( record.getInjury( 0 ).getX_sequence_D().getValidity(), 1 ).toUpperCase();
            if (ArrayUtils.contains( new String[]{ "Y", "N", "U" }, validity )) {
                ifnData.getDtwc().setQuestionTheValidityOfTheClaim( validity );
            }
        }
        // if ( record.countInjury() > 0)
        ifnData.getDtwc().setEmployeeMissWork( "U" );
        try {
            Date dt = DateUtils.parseDate( record.getDisabilityStartDate(), IN_DATE_FORMAT );
            ifnData.getDtwc().setDisabilityDate( DateFormatUtils.format( dt, OUT_DATE_FORMAT ) );
        } catch ( java.text.ParseException pex ) {
            // ignore
        }
        if (StringUtils.isNotEmpty( record.getPaidinFullIndicator() )) {
            ifnData.getDtwc().setFullPayForDayOfInjury( StringUtils.left( record.getPaidinFullIndicator(), 1 ) );
        }
        if (StringUtils.isNotEmpty( record.getAverageHoursWorked() ) && StringUtils.isNumeric( record.getAverageHoursWorked() )) {
            ifnData.getDtwc().setWeeklyHours( record.getAverageHoursWorked() );
        }
        if (StringUtils.isNotEmpty( record.getNumberofDependents() )) {
            ifnData.getDtwc().setNumberOfDependents( record.getNumberofDependents() );
        }
        ifnData.getDtwc().setAdmittedToHospital( "U" );
        ifnData.getDtwc().setStillInHospital( "U" );
        if (StringUtils.isNotEmpty( record.getCallerFirstName() )) {
            ifnData.getDtwc().setReportPreparedByFirstName( StringUtils.upperCase( record.getCallerFirstName() ) );
        }
        if (StringUtils.isNotEmpty( record.getCallerLastName() )) {
            ifnData.getDtwc().setReportPreparedByLastName( StringUtils.upperCase( record.getCallerLastName() ) );
        }
        if (StringUtils.isNotEmpty( record.getClientAddress() )) {
            if (record.getClientAddress().length() > 30) {
                ifnData.getDtwc().setReportPreparedByAddressLine1( StringUtils.upperCase( StringUtils.substring( record.getClientAddress(), 0, 30 ) ) );
                ifnData.getDtwc().setReportPreparedByAddressLine2( StringUtils.upperCase( StringUtils.substring( record.getClientAddress(), 30 ) ) );
            } else {
                ifnData.getDtwc().setReportPreparedByAddressLine1( StringUtils.upperCase( record.getClientAddress() ) );
            }
        }
        if (StringUtils.isNotEmpty( record.getClientCity() )) {
            ifnData.getDtwc().setReportPreparedByCity( StringUtils.upperCase( record.getClientCity() ) );
        }
        if (StringUtils.isNotEmpty( record.getClientState() )) {
            ifnData.getDtwc().setReportPreparedByState( record.getClientState() );
        }
        if (StringUtils.isNotEmpty( record.getClientZip() )) {
            ifnData.getDtwc().setReportPreparedByPostalCode( checkZip( record.getClientZip() ) );
        }
        if (StringUtils.isNotEmpty( record.getJobTitle() )) {
            ifnData.getDtwc().setReportPreparedByJobTitle( StringUtils.upperCase( record.getJobTitle() ) );
        }
        ifnData.getDtwc().setReferredByWhom( REFERRED_BY_WHOM );
    }

    // mapDTWC
    private void mapEEDT( Claim ifnData, X_sequence_B record )
    {
        ifnData.getEedt().setIncidentNumber( record.getTransactionId() );
        ifnData.getEedt().setClaimantLastName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientLastName() ) ) );
        ifnData.getEedt().setClaimantFirstName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientFirstName() ) ) );
        ifnData.getEedt().setClaimantMiddleInitial( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientMiddleName() ) ) );
        ifnData.getEedt().setClaimantAddressLine1( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientAddress() ) ) );
        ifnData.getEedt().setClaimantAddressLine2( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientAddress2() ) ) );
        ifnData.getEedt().setClaimantAddressCity( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientCity() ) ) );
        ifnData.getEedt().setClaimantAddressState( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientState() ) ) );
        if (StringUtils.isNotEmpty( record.getPatientZip() )) {
            String zip = checkZip( record.getPatientZip() );
            if (zip.length() == 6 && !zip.matches( CANADA_ZIP_EXP )) {
                zip = "000000";
            }
            ifnData.getEedt().setClaimantAddressPostalCode( zip );
        }
        ifnData.getEedt().setClaimantCounty( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientCounty() ) ) );
        ifnData.getEedt().setClaimantAge( StringUtils.trimToEmpty( record.getPatientAge() ) );
        ifnData.getEedt().setClaimantPhoneNumber( checkPhone( StringUtils.trimToEmpty( record.getPatientHomeTelephone() ) ) );
        ifnData.getEedt().setClaimantCellPhone( checkPhone( StringUtils.trimToEmpty( record.getPatientCellNumber() ) ) );
        ifnData.getEedt().setClaimantEMailAddress( StringUtils.upperCase( StringUtils.trimToEmpty( record.getPatientEmail() ) ) );
        ifnData.getEedt().setClaimantEmployeeId( StringUtils.upperCase( StringUtils.trimToEmpty( record.getEmployeeID() ) ) );
        ifnData.getEedt().setClaimantDateOfBirth( StringUtils.trimToEmpty( record.getPatientDateOfBirth() ) );
        ifnData.getEedt().setClaimantGenderCode( StringUtils.trimToEmpty( record.getPatientGender() ) );
        String maritalStatus = StringUtils.trimToEmpty( record.getMaritalStatus() );
        String statusCode = "U";
        if (maritalStatus.equalsIgnoreCase( "Divorced" )) {
            statusCode = "D";
        } else if (maritalStatus.equalsIgnoreCase( "Married" )) {
            statusCode = "M";
        } else if (maritalStatus.equalsIgnoreCase( "Single" )) {
            statusCode = "S";
        } else if (maritalStatus.equalsIgnoreCase( "Widow" )) {
            statusCode = "W";
        }
        ifnData.getEedt().setClaimantMaritalStatusCode( statusCode );
        ifnData.getEedt().setClaimantSsn( StringUtils.trimToEmpty( record.getPatientSSN() ) );
        ifnData.getEedt().setSupervisorFirstName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getSupervisorFirstName() ) ) );
        ifnData.getEedt().setSupervisorLastName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getSupervisorLastName() ) ) );
        ifnData.getEedt().setSupervisorWorkPhone( checkPhone( StringUtils.trimToEmpty( record.getSupervisorTelephoneNumber() ) ) );
        ifnData.getEedt().setHireDate( StringUtils.trimToEmpty( record.getHireDate() ) );
        ifnData.getEedt().setEmploymentStatus( StringUtils.trimToEmpty( record.getEmploymentStatus() ) );
        ifnData.getEedt().setEmployeeJobCode( StringUtils.trimToEmpty( record.getJobCode() ) );
        ifnData.getEedt().setJobTitlePosition( StringUtils.upperCase( StringUtils.trimToEmpty( record.getJobTitle() ) ) );
        ifnData.getEedt().setPayType( "H" );
        ifnData.getEedt().setWage( StringUtils.leftPad( StringUtils.trimToEmpty( record.getHourlyWage() ), 11, "0" ) );
    }

    // mapEEDT
    private void mapSSQD( Claim ifnData, X_sequence_B record )
    {
        ifnData.getSsqd().setIncidentNumber( record.getTransactionId() );
        String state = null;
        state = StringUtils.trimToEmpty( record.getClientState() );
        if (state.equalsIgnoreCase( "TX" )) {
            String patientLanguage = null;
            patientLanguage = StringUtils.trimToEmpty( record.getPatientLanguage() );
            if (patientLanguage.equalsIgnoreCase( "*English" )) {
                ifnData.getSsqd().setEmployeeSpeakEnglish( "Y" );
            } else {
                ifnData.getSsqd().setEmployeeSpeakEnglish( "N" );
            }
        } else {
            ifnData.removeSsqd();
            // SSOD record only for TX claim
        }
    }

    private void mapHOSD( Claim ifnData, X_sequence_B record )
    {
        ifnData.getHosd( 0 ).setIncidentNumber( record.getTransactionId() );
        ifnData.getHosd( 0 ).setSequenceNumber( "001" );
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderName() )) {
            ifnData.getHosd( 0 ).setHospitalName( StringUtils.upperCase( record.getInjury( 0 ).getX_sequence_D().getProviderName() ) );
        } else {
            ifnData.getHosd( 0 ).setHospitalName( StringUtils.upperCase( record.getInjury( 0 ).getX_sequence_D().getProviderName2() ) );
        }
        String hospitalAddress = "";
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderAddress() )) {
            hospitalAddress = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderAddress() ) );
        } else {
            hospitalAddress = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderAddress2() ) );
        }
        if (StringUtils.isNotBlank( hospitalAddress )) {
            if (hospitalAddress.length() > 30) {
                ifnData.getHosd( 0 ).setHospitalAddressLine1( hospitalAddress.substring( 0, 30 ) );
                ifnData.getHosd( 0 ).setHospitalAddressLine2( hospitalAddress.substring( 30 ) );
            } else {
                ifnData.getHosd( 0 ).setHospitalAddressLine1( hospitalAddress );
            }
        }
        String hospitalCity = "";
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderCity() )) {
            hospitalCity = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderCity() ) );
        } else {
            hospitalCity = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderCity2() ) );
        }
        if (StringUtils.isNotBlank( hospitalCity )) {
            ifnData.getHosd( 0 ).setHospitalCity( hospitalCity );
        }
        String hospitalState = "";
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderState() )) {
            hospitalState = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderState() ) );
        } else {
            hospitalState = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderState2() ) );
        }
        if (StringUtils.isNotBlank( hospitalState )) {
            ifnData.getHosd( 0 ).setHospitalState( hospitalState );
        }
        String hospitalZip = "";
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderZip() )) {
            hospitalZip = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderZip() );
        } else {
            hospitalZip = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderZip2() );
        }
        if (StringUtils.isNotBlank( hospitalZip )) {
            ifnData.getHosd( 0 ).setHospitalPostalCode( checkZip( hospitalZip ) );
        }
        String hospitalPhone = "";
        if (StringUtils.isNotBlank( record.getInjury( 0 ).getX_sequence_D().getProviderTelephoneNumber() )) {
            hospitalPhone = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderTelephoneNumber() );
        } else {
            hospitalPhone = StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getProviderTelephoneNumber2() );
        }
        if (StringUtils.isNotBlank( hospitalPhone )) {
            ifnData.getHosd( 0 ).setHospitalPhoneNumber( checkPhone( hospitalPhone ) );
        }
        ifnData.getHosd( 0 ).setHospitalInformationAvailable( "Y" );
    }

    private void mapWITS( Claim ifnData, X_sequence_B record )
    {
        ifnData.getWits().setIncidentNumber( record.getTransactionId() );
        ifnData.getWits().setSequenceNumber( "1" );
        String witnessName = StringUtils.upperCase( StringUtils.trimToEmpty( record.getInjury( 0 ).getX_sequence_D().getWitnessName() ) );
        if (!ArrayUtils.contains( WITNESS_FILTER, witnessName )) {
            if (StringUtils.contains( witnessName, ' ' )) {
                ifnData.getWits().setWitnessFirstName( StringUtils.substringBefore( witnessName, " " ) );
                ifnData.getWits().setWitnessLastName( StringUtils.substringAfterLast( witnessName, " " ) );
            } else {
                ifnData.getWits().setWitnessLastName( witnessName );
            }
        }
        ifnData.getWits().setWitnessWorkPhoneNumber( checkPhone( record.getInjury( 0 ).getX_sequence_D().getWitnessWorkTelephone() ) );
    }

    private void mapASID( Claim ifnData, X_sequence_B record, otdDbNT24IFN.OtdDbNT24IFNOTD in, String recordXml )
        throws Exception
    {
        in.getPS_SEL_ASI_map().executeQuery();
        if (in.getPS_SEL_ASI_map().resultsAvailable()) {
            int asidIdx = 0;
            String strTag = "";
            String strQuestionValue = "";
            String strTagValue = "";
            while (in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().next()) {
                if (StringUtils.isNotBlank( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getClient_Id() )) {
                    if (!record.getBusinessIndustryAccountID().equalsIgnoreCase( StringUtils.trim( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getClient_Id() ) )) {
                        continue;
                    }
                }
                if (StringUtils.trim( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getASI_Field() ).equalsIgnoreCase( ASI_ABM_ADR )) {
                    String surveyQuestion = in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getQuestion_value().toUpperCase().trim();
                    boolean bQuestionMatch = false;
                    boolean bAnswerMatch = false;
                    String strQuestion = "";
                    String strAnswer = "";
                    for (int i = 0; i < record.getSurvey().countX_choice_C(); i++) {
                        if (!bQuestionMatch && record.getSurvey().getX_choice_C( i ).hasQuestion()) {
                            strQuestion = StringUtils.trimToEmpty( record.getSurvey().getX_choice_C( i ).getQuestion() );
                            if (strQuestion.equalsIgnoreCase( surveyQuestion )) {
                                bQuestionMatch = true;
                            }
                            strAnswer = "";
                            if (StringUtils.isNotEmpty( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value() )) {
                                strAnswer = StringUtils.upperCase( StringUtils.trimToEmpty( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value().substring( 0, 1 ) ) );
                                if (ArrayUtils.contains( SURVEY_ANSWER, strAnswer )) {
                                    bAnswerMatch = true;
                                }
                            }
                        }
                        if (bQuestionMatch && !bAnswerMatch && record.getSurvey().getX_choice_C( i ).hasAnswer()) {
                            strAnswer = StringUtils.upperCase( StringUtils.trimToEmpty( record.getSurvey().getX_choice_C( i ).getAnswer().substring( 0, 1 ) ) );
                            if (ArrayUtils.contains( SURVEY_ANSWER, strAnswer )) {
                                bAnswerMatch = true;
                            } else {
                                bQuestionMatch = false;
                            }
                        }
                        if (bQuestionMatch && bAnswerMatch) {
                            ifnData.getAsid( asidIdx ).setIncidentNumber( record.getTransactionId() );
                            ifnData.getAsid( asidIdx ).setSequenceNumber( StringUtils.leftPad( String.valueOf( asidIdx + 1 ), 3, '0' ) );
                            ifnData.getAsid( asidIdx ).setIfnAttributeName( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getASI_Field().toUpperCase() );
                            ifnData.getAsid( asidIdx ).setValueForAsiQuestion( strAnswer );
                            asidIdx++;
                            break;
                        }
                    }
                    // for
                } else {
                    ifnData.getAsid( asidIdx ).setIncidentNumber( record.getTransactionId() );
                    ifnData.getAsid( asidIdx ).setSequenceNumber( StringUtils.leftPad( String.valueOf( asidIdx + 1 ), 3, '0' ) );
                    ifnData.getAsid( asidIdx ).setIfnAttributeName( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getASI_Field().toUpperCase() );
                    if (StringUtils.trim( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getEdi_Tag() ).equalsIgnoreCase( NO_TAG )) {
                        if (StringUtils.isNotEmpty( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value() )) {
                            ifnData.getAsid( asidIdx ).setValueForAsiQuestion( StringUtils.substring( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value(), 0, in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getMax_len() ) );
                        }
                    } else {
                        strTagValue = StringUtils.substringBetween( recordXml, in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getEdi_Tag().trim(), "</" );
                        if (StringUtils.isNotEmpty( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getQuestion_value() )) {
                            strQuestionValue = StringUtils.trim( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getQuestion_value() );
                            if (strQuestionValue.equalsIgnoreCase( strTagValue )) {
                                if (StringUtils.isNotEmpty( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value() )) {
                                    ifnData.getAsid( asidIdx ).setValueForAsiQuestion( StringUtils.substring( in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getDefault_value(), 0, in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getMax_len() ) );
                                }
                            } else {
                                ifnData.getAsid( asidIdx ).setValueForAsiQuestion( StringUtils.substring( strTagValue, 0, in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getMax_len() ) );
                            }
                        } else {
                            ifnData.getAsid( asidIdx ).setValueForAsiQuestion( StringUtils.substring( strTagValue, 0, in.getPS_SEL_ASI_map().get$PS_SEL_ASI_mapResults().getMax_len() ) );
                        }
                    }
                    asidIdx++;
                }
            }
            // where
        }
    }

    private void mapCOND( Claim ifnData, X_sequence_B record )
    {
        ifnData.getCond( 0 ).setIncidentNumber( record.getTransactionId() );
        ifnData.getCond( 0 ).setSequenceNumber( "001" );
        ifnData.getCond( 0 ).setContactFirstName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getSupervisorFirstName() ) ) );
        ifnData.getCond( 0 ).setContactLastName( StringUtils.upperCase( StringUtils.trimToEmpty( record.getSupervisorLastName() ) ) );
        ifnData.getCond( 0 ).setContactAddressLine1( StringUtils.upperCase( StringUtils.trimToEmpty( record.getClientAddress() ) ) );
        ifnData.getCond( 0 ).setContactCity( StringUtils.upperCase( StringUtils.trimToEmpty( record.getClientCity() ) ) );
        ifnData.getCond( 0 ).setContactState( StringUtils.upperCase( StringUtils.trimToEmpty( record.getClientState() ) ) );
        ifnData.getCond( 0 ).setContactPostalCode( checkZip( record.getClientZip() ) );
        ifnData.getCond( 0 ).setContactWorkPhoneNumber( checkPhone( record.getSupervisorTelephoneNumber() ) );
        ifnData.getCond( 0 ).setContactType( "EMP" );
    }

    public String checkZip( String inZip )
    {
        String zip = StringUtils.remove( StringUtils.trimToEmpty( inZip ), ' ' );
        String outZip = "00000";
        if (zip.length() == 5 || zip.length() == 6) {
            outZip = zip;
        } else if (zip.length() > 6) {
            outZip = StringUtils.left( zip, 5 );
        }
        return outZip;
    }

    public String checkPhone( String phoneNum )
    {
        String retPhoneNum = "";
        if (StringUtils.isNotBlank( phoneNum )) {
            retPhoneNum = phoneNum.replaceAll( "[^0-9]", "" );
        }
        return retPhoneNum;
    }

    // =====================================================
    public String validateTpAttribute( xsd.eX_TPAttribute1628188874.TPAttribute attrib )
    {
        StringBuffer error = new StringBuffer();
        if (!attrib.hasINFILE()) {
            error.append( "INFILE" + " not found; " );
        }
        if (!attrib.hasFILETRACKERID()) {
            error.append( "FILETRACKERID" + " not found; " );
        }
        if (!attrib.hasXFILENAME()) {
            error.append( "XFILENAME" + " not found; " );
        }
        if (!attrib.hasEVENTNAME()) {
            error.append( "EVENTNAME" + " not found; " );
        }
        return error.length() > 0 ? error.toString() : null;
    }

    public String formatAttribMsg( xsd.eX_TPAttribute1628188874.TPAttribute attrib )
    {
        if (attrib == null) {
            return null;
        } else {
            StringBuffer msg = new StringBuffer();
            msg.append( "\ninFile: " + attrib.getINFILE() );
            msg.append( "\nFile Tracker Id: " + attrib.getFILETRACKERID() );
            msg.append( "\nxFilename: " + attrib.getXFILENAME() );
            msg.append( "\nEvent Name: " + attrib.getEVENTNAME() );
            return msg.toString();
        }
    }

    public void errorEvent( com.stc.connectors.jms.JMS jmsProcesHandler, xsd.xsdProcessHandler_1006081694.ProcessHandler processHandler, xsd.eX_TPAttribute1628188874.TPAttribute tpAttribute, String eventNum, String eventMsg )
        throws java.io.IOException, javax.jms.JMSException
    {
        processHandler.setEvent_Number( eventNum );
        processHandler.getProcessTrackingRec().getX_sequence_C().setActivity_Type( 4 );
        processHandler.getProcessTrackingRec().getX_sequence_C().setActivity_Desc( "Mapping Exception" );
        processHandler.getErrorEventRec().getX_sequence_F().setEvent_Type_Name( tpAttribute.getEVENTNAME() );
        processHandler.getErrorEventRec().getX_sequence_F().setCollaboration_Rule_Name( this.collabContext.getCollaborationName() );
        processHandler.getErrorEventRec().getX_sequence_F().setEway_Name( this.collabContext.getConnectivityMapName() );
        processHandler.getErrorEventRec().getX_sequence_F().setError_Number( "CVTY_" + eventNum );
        processHandler.getErrorEventRec().getX_sequence_F().setError_Severity( "E" );
        processHandler.getErrorEventRec().getX_sequence_F().setError_Message( eventMsg );
        // compose notification
        String emailSubject = "SeeBeyond Exception - " + this.collabContext.getCollaborationName() + " Trading Partner: " + processHandler.getTP_Logical();
        String timestamp = DateFormatUtils.format( new Date(), ERROR_TIMESTAMP_FORMAT );
        String emailBody = "Timestamp: " + timestamp + "\nCollaboration Name: " + this.collabContext.getCollaborationName() + "\nError: " + eventMsg + formatAttribMsg( tpAttribute );
        processHandler.getEmailEventRec().getX_sequence_E().setSource_System( SOURCE_SYSTEM );
        processHandler.getEmailEventRec().getX_sequence_E().setTarget_System( TARGET_SYSTEM );
        processHandler.getEmailEventRec().getX_sequence_E().setSubject( emailSubject );
        processHandler.getEmailEventRec().getX_sequence_E().setBody( emailBody );
        jmsProcesHandler.sendText( processHandler.marshalToString() );
    }
    
    public String receiveAndMap(@Body String body) throws Throwable {
    	StringBuffer result = new StringBuffer ();
    	byte[] sampleBytes = wrap(body.getBytes());
    	this.receive(result, new String(sampleBytes, "UTF-8"), null, new com.stc.targetFields1442425817.Root (), new xsd.eX_TPAttribute1628188874.TPAttribute (), new xsd.cvtyStdEvent_795650394.ExStdEvent (), null, null , new xsd.EDI_1226447247.NurseTriage24x7 (), new xsd.xsdProcessHandler_1006081694.ProcessHandler (), new otdDbNT24IFN.OtdDbNT24IFNOTD ());
    	return result.toString();
    }
	
}
