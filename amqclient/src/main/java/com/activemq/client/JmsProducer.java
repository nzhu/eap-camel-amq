package com.activemq.client;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;


public class JmsProducer {
	public void send (String payload) { 
		
		// Create a ConnectionFactory
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		connectionFactory.setUserName("admin");
		connectionFactory.setPassword("jb0ssr@cks");
		// Create a Connection
		Connection connection;
		try {
			connection = connectionFactory.createConnection();

		// Create a Session
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		//ObjectMessage msg = session.createObjectMessage();
		TextMessage msg = session.createTextMessage(payload);

		//msg.setJMSType();
		// Create the destination
		
		Destination destination = session.createQueue("DATA_IN");

		// Create a MessageProducer from the Session to the Queue
		MessageProducer producer = session.createProducer(destination);

		// Start the connection
		connection.start();
		
		producer.send(msg);
		
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	
	
	public static void main (String args[]) {
		JmsProducer producer = new JmsProducer();
		
		StringBuffer payload = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\"?><NurseTriage24x7><Record><EDIReferenceNumber>391523</EDIReferenceNumber><TransactionId>DKR0000Q</TransactionId><CallerFirstName>Testfirst</CallerFirstName><CallerLastName>Callos Resource</CallerLastName><SupervisorFirstName>TEST</SupervisorFirstName><SupervisorLastName>TEST</SupervisorLastName><SupervisorTelephoneNumber>2101112222</SupervisorTelephoneNumber><TransactionCallType>Inbound</TransactionCallType><TransactionCallDate>20140911</TransactionCallDate><TransactionCallTime>093527</TransactionCallTime><TransactionCallTimeZone>CDT</TransactionCallTimeZone><TransactionCallPathway>Triage</TransactionCallPathway><RelationshipToCaller></RelationshipToCaller><CallReason>Testing the POI Verbiage wording and ESIS/IFN translation</CallReason><Jurisdiction>TX</Jurisdiction><InjuryDescription></InjuryDescription><PatientFirstName>Testfirst</PatientFirstName><PatientMiddleName>xx</PatientMiddleName><PatientLastName>Callos Resource</PatientLastName><PatientAddress>123 Test Street</PatientAddress><PatientCity>San Antonio</PatientCity><PatientState>TX</PatientState><PatientZip>78251</PatientZip><PatientWorkTelephone></PatientWorkTelephone><PatientHomeTelephone>2101112222</PatientHomeTelephone><PatientSSN>111221111</PatientSSN><PatientGender>F</PatientGender><PatientDateOfBirth>19700101</PatientDateOfBirth><PatientLanguage>*English</PatientLanguage><EmployeeID>123456</EmployeeID><JobTitle>Maintenance/Engineering</JobTitle><OtherJob></OtherJob><HourlyWage>0</HourlyWage><AverageHoursWorked>0</AverageHoursWorked><PayorLocation>0</PayorLocation><BusinessIndustryID>CALLO</BusinessIndustryID><BusinessIndustryAccountID>CLS</BusinessIndustryAccountID><BusinessIndustryName>CALLOS RESOURCE</BusinessIndustryName><BusinessIndustryAddress></BusinessIndustryAddress><BusinessIndustryCity></BusinessIndustryCity><BusinessIndustryState></BusinessIndustryState><BusinessIndustryZip></BusinessIndustryZip><BusinessIndustryTelephoneNumber></BusinessIndustryTelephoneNumber><BusinessIndustryFaxNumber></BusinessIndustryFaxNumber><ClientID>838000</ClientID><ClientName>CALLOS RESOURCE - DEFAULT LOCATION - DEFAULT</ClientName><ClientAddress></ClientAddress><ClientCity></ClientCity><ClientState></ClientState><ClientZip></ClientZip><ClientTelephoneNumber></ClientTelephoneNumber><ClientFaxNumber></ClientFaxNumber><ExternalAccountNumber>29605166</ExternalAccountNumber><DivisionCode></DivisionCode><RegionCode></RegionCode><Interpreter>NR</Interpreter><NumberofDependents>0</NumberofDependents><PatientFaxNumber></PatientFaxNumber><PatientEmail></PatientEmail><PatientAddress2></PatientAddress2><PatientCounty>Bexar</PatientCounty><PatientCellNumber></PatientCellNumber><PatientAge>45</PatientAge><PatientAgeMonths>4</PatientAgeMonths><MaritalStatus></MaritalStatus><HireDate>20140904</HireDate><EmploymentStatus></EmploymentStatus><JobCode>MAINT</JobCode><ReportedByPhoneNo></ReportedByPhoneNo><DisabilityStartDate></DisabilityStartDate><PaidinFullIndicator></PaidinFullIndicator><InjuryID></InjuryID><Filler01></Filler01><Filler02></Filler02><Filler03></Filler03><Filler04></Filler04><Filler05></Filler05><Survey></Survey><Injury><TriageRecordID>DKR0000Q</TriageRecordID><TriageCallDate>20140911</TriageCallDate><TriageCallTime>093600</TriageCallTime><TriageCallTimeZone>CDT</TriageCallTimeZone><InjuryMainComplaint>ACHES</InjuryMainComplaint><RelatedComplaint></RelatedComplaint><RelatedComplaint2></RelatedComplaint2><RelatedComplaint3></RelatedComplaint3><HealthHistory></HealthHistory><InjuryDateTime>20140911 093715</InjuryDateTime><InjuryType>CRUSHING</InjuryType><InjuredBodyPart>EAR(S)</InjuredBodyPart><InjuryLocation>Lobby</InjuryLocation><WeightLifted>0</WeightLifted><HeightofFall>0</HeightofFall><EquipmentUtilized>Fork Lift</EquipmentUtilized><AssistiveDevice>Shoes for Crews</AssistiveDevice><Predisposition>Call 911</Predisposition><Disposition>Go to Urgent Care</Disposition><OverrideLevel></OverrideLevel><OverrideReason></OverrideReason><Compliance>Yes</Compliance><NonComplianceReason></NonComplianceReason><FollowUpDisposition></FollowUpDisposition><CallNotes>TEST Notes</CallNotes><WitnessName>None</WitnessName><GuidelineUsed1>Back Pain (Adult After Hours)</GuidelineUsed1><GuidelineUsed2></GuidelineUsed2><GuidelineUsed3></GuidelineUsed3><GuidelineUsed4></GuidelineUsed4><ProviderName></ProviderName><ProviderType></ProviderType><ProviderAddress></ProviderAddress><ProviderCity></ProviderCity><ProviderState></ProviderState><ProviderZip></ProviderZip><ProviderTelephoneNumber></ProviderTelephoneNumber><RecordingNumber>123456</RecordingNumber><AdditionalRecordingNumber></AdditionalRecordingNumber><AssistiveDevice2></AssistiveDevice2><AssistiveDevice3></AssistiveDevice3><InjuryTimeZone>Central Daylight Time</InjuryTimeZone><NotifiedDateTime>2014-09-11 09:36:00</NotifiedDateTime><NotifiedTimeZone>Central Standard Time</NotifiedTimeZone><MilestoWork>0</MilestoWork><InjuredOnPremises>Yes</InjuredOnPremises><AccidentAddress></AccidentAddress><AccidentCity></AccidentCity><AccidentState></AccidentState><AccidentZip></AccidentZip><ReturnToWorkProgram></ReturnToWorkProgram><WitnessWorkTelephone></WitnessWorkTelephone><HCRINo></HCRINo><InitialTreatmentCode></InitialTreatmentCode><BodyPartCode>13</BodyPartCode><InjuryCode>13</InjuryCode><CauseOfInjuryCode>13</CauseOfInjuryCode><CauseOfInjury>CAUGHT IN, UNDER, OR BETWEEN: MISCELLANEOUS</CauseOfInjury><CallingFrom></CallingFrom><WorkLocationCode></WorkLocationCode><WorkLocation></WorkLocation><PrivateArea>No</PrivateArea><Validity></Validity><SideofBody>Left</SideofBody><ProviderName2></ProviderName2><ProviderType2></ProviderType2><ProviderAddress2></ProviderAddress2><ProviderCity2></ProviderCity2><ProviderState2></ProviderState2><ProviderZip2></ProviderZip2><ProviderTelephoneNumber2></ProviderTelephoneNumber2><ProviderName3></ProviderName3><ProviderType3></ProviderType3><ProviderAddress3></ProviderAddress3><ProviderCity3></ProviderCity3><ProviderState3></ProviderState3><ProviderZip3></ProviderZip3><ProviderTelephoneNumber3></ProviderTelephoneNumber3><TextPermission></TextPermission><TextCellPhone></TextCellPhone><TextProvider></TextProvider><TextNotSentReason></TextNotSentReason><Filler06></Filler06><Filler07></Filler07><Filler08></Filler08><Filler09></Filler09><Filler10></Filler10></Injury></Record></NurseTriage24x7>");
		producer.send(payload.toString());
		
		System.out.println ("Message Sent to AMQ Completed");
		System.exit(0);
	}
}
